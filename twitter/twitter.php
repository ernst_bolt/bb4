<!--
Deze pagina laat zien hoe je een twitter widget kunt toevoegen aan je pagina.
Er is gebruikt gemaakt van de bxslider:
http://bxslider.com/examples/image-slideshow-captions
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../css/table.css" rel="stylesheet" type="text/css"/>
        <link href="lib/bxslider-4-master/jquery.bxslider.css" rel="stylesheet" type="text/css"/>

        <!--
        JQUERY BIBLIOTHEEK WORDT OPGEHAALD BIJ GOOGLE
        -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#bxslider').bxSlider({
                        
                    mode: 'fade',
                    speed: 1000,
                    pause: 5000,
                    auto: 'true',
                    
                    captions: true
                });
            });
        </script>

    </head>
    <body>
        <div id="container">


            <div id="header">  

                <h1>Twitter widget voorbeeld</h1>

                <div id="menu">  


                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li><a href="http://www.wereldbolt.nl" target="blank">wereldbolt</a></li>
                        <li><a href="eigenpagina.html">mijn super geheime pagina</a></li>
                    </ul>


                </div>
            </div>

            <div id="content">  

                <ul id='bxslider'>
                    <li><img src="images/login.png" title="login" /></li>
                    <li><img src="images/settings.png" title="ga naar settings"/></li>
                    <li><img src="images/widgets.png" title="click op widgets"/></li>
                    <li><img src="images/createnew.png" title="click op 'create new'"/></li>
                    <li><img src="images/createwidget.png" title="pas de velden aan en click op 'create widget'" /></li>
                    <li><img src="images/savepaste.png" title="click op save en kopieer de code in je website"/></li>
                </ul>

            </div>
            <div id="footer"><h4>Loving bxslider</h4> </div>         

        </div>
    </body>
</html>

