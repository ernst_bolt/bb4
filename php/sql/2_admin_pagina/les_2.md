# Inleiding #
Het is erg belangrijk dat gebruikers kunnen inloggen op je pagina. Daarvoor maken we deze les een plan. 
 
## Doel 
Het controleren van de ontvangen gebruikersnaam en wachtwoord met de gegevens in de database.


# Controle 
Gegevens uit `$_POST` kun je controleren met gegevens die je opvraagt uit de database. Dit klinkt eenvoudig maar het is erg belangrijk dat er een goed plan wordt gevolgd zodat je zeker weet dat het veilig is en klopt.   
    
# Opdrachten voor in de les 

1) Maak een activity diagram voor het inloggen. 

2) Vertaal het activity diagram in functies.


# Huiswerk 
 
Doel: Een admin-pagina maken waarop alle gebruikers te zien zijn.

1) Maak een pagina admin.php

2) Maak de functies uit `sql.php` beschikbaar in admin.php. Gebruik daarvoor `require_once`. In `index.php` van les 1 is dit voor gedaan. 
 
3) Maak een variabele `$gebruikers = ...`. 
 
4) In het bestand `sql.php` staat een functie `select`. Gebruik deze functies om gebruikers op te halen uit de database: `$gebruikers = select(..,..);`.
 
5) De functie krijgt twee parameters mee. Als je niet weet welke SQL je moet meegeven: er staat een voorbeeld boven de functie in `sql.php`. 
 
6) Print de gebruikers op het scherm. Een voorbeeld van een lijst uitprinten staat in het bestand `index.php` uit de vorige les. 

