# Inleiding #
Een geregistreerd gebruiker moet de mogelijkheid hebben om uit te loggen. In de vorige les heb je gemaakt dat een geregistreerde gebruiker is ingelogd en wordt onthouden in de sessie. In deze les zorg je ervoor dat een geregistreerde gebruiker kan uitloggen.
 
## Doel 
* Het vernietigen van de sessie, waardoor een gebruiker is uitgelogd.    
* Het doorverwijzen van een gebruiker na uitloggen.
    
# Opdrachten voor in de les 

1) Maak een bestand logout.php

2) Zorg ervoor dat de sessie vervolgt wordt met `session_start()`. 

3) Met de volgende code kun je de sessie vernietigen: `session_destroy();`. Voeg deze code toe onder `session_start();`.


# Huiswerk 
 
Doel: Verwijs de gebruiker door naar de homepage zodra deze is uitgelogd en geef een passende melding

1) Gebruik de function `header('Location: hompage.php');` om de gebruiker door te verwijzen naar de homepage.

2) Voeg een queryparameter toe aan de url waarnaar je verwijst. De queryparameter bevat de boodschap die aan de gebruiker getoond wordt. Bijvoorbeeld: `homepage.php?bericht=Je%20bent%20uitgelogd`. 
 
3) Lees de queryparameter op je homepage uit. Vergeet niet te controleren of de queryparameter bestaat, gebruik daarvoor `isset(..)`. 
 
4) Toon het bericht op de homepage als de gebruiker bestaat.