<?php
/*
Versie: 0.2
*/


//Configuratie
function getConfiguration(){
	
	$db_servername = 'localhost';
	$db_username = 'root';
	$db_password = 'usbw';
	$db_name = 'hobby';

	$config = array(

		'db_servername' => $db_servername,
		'db_username' => $db_username,
		'db_password' => $db_password,
		'db_name' => $db_name

	);

	return $config;
}

//Create connection 
function getConnection(){

	$config = getConfiguration();
	$servername = $config['db_servername'];
	$username = $config['db_username'];
	$password = $config['db_password'];
	$db = $config['db_name'];
	
	$conn = mysqli_connect($servername, $username, $password, $db);

	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	return $conn;
}

//Close connection
function closeConnection($conn){

		mysqli_close($conn);
}

/*Insert

SQL voorbeeld: "INSERT INTO MyGuests (firstname, lastname, email) VALUES ('John', 'Doe', 'john@example.com')"
Functie aanroep voorbeeld: insert($conn, $sql);

*/
function insert($conn, $sql){
	
	if (!mysqli_query($conn, $sql)) {
	    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	    return false;
	}else{
		return true;
	}

}

/*
Select
$sql = "SELECT id, firstname, lastname FROM MyGuests";
*/
function select($conn, $sql){

	$result = mysqli_query($conn, $sql);
	return $result;
}

function printSelect($result, $veldNaam){

	if (mysqli_num_rows($result) > 0) {
		// output data of each row
		while($row = 	mysqli_fetch_assoc($result)) 	{
	    	echo "id: " . 	$row["id"] . $veldNaam . $row[$veldNaam] . "<br>";
		}
	} else {
		echo "0 results";
	}

}

/*
Delete
DELETE FROM Klant WHERE Naam='Alfreds Futterkiste';
*/
function delete($conn, $sql){
	if ($conn->query($sql) === TRUE) {
	    echo "Record deleted successfully";
	} else {
	    echo "Error deleting record: " . $conn->error;
	}
}

?>