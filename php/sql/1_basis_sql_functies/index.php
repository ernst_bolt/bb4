
<?php


/*
Opdrachten

1) Download HeidiSQL
2) Maak een database: hobby
3) Maak een tabel: feedback
4) Geef de tabel de volgende velden: id (auto increment en primary key), naam (varchar), bericht (varchar)
5) Ga naar het bestand sql.php, en controleer de gegevens in de getConfig-functie.
6) Voer onderstaande test functies uit

*/

require_once 'sql.php';

$conn = getConnection();
$insert = "INSERT INTO feedback (naam, bericht) VALUES ('meneer Bolt', 'Gefeliciteerd! Het is je gelukt om gegevens op te slaan en weer op te halen! JOEHOEEE!')";
insert($conn, $insert);

$select = "SELECT * FROM feedback";
$result = select($conn, $select);
printSelect($result, 'bericht');

closeConnection($conn);


?>
