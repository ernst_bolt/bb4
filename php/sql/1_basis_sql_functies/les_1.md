# Inleiding #
Het leukste aan de interactie met gebruikers is dat je ook data kunt opslaan. Dit kan op verschillende manieren. In deze les maak je gebruik van een database.  
 
## Doel 
Het opslaan van gegevens in een database, die meegegeven zijn door een gebruiker via de browser.


# Webserver en database server 
Eerder heb je kennisgemaakt met een webserver: de plaats waar je gegevens opvraagt, en die door de server verwerkt worden.   
  
De volgende stap is dat een de webserver communiceert met een database server. De databaseserver heeft databases (verzamelingen) waar je gegevens in kunt bewaren. Een database bestaat uit tabellen.  
# Opdrachten voor in de les 

1) Download HeidiSQL 

2) Maak een database: hobby 

3) Maak een tabel: feedback 

4) Geef de tabel de volgende velden: id (auto increment en primary key), naam (varchar), bericht (varchar) 

5) Download de bestanden index.php en sql.php van Bitbucket. Verplaats de bestanden naar je root folder.

6) Ga naar het bestand sql.php, en controleer de gegevens in de getConfiguration-functie. 

7) Verplaats index.php en sql.php naar je webserver. Start de webserver en controleer of er in je database een record is gemaakt.

# Huiswerk 
 
Doel: een registratie-formulier maken waarvan de gegevens opgeslagen worden in de database. 

1) Maak in de database een tabel `gebruiker`. 
 
2) Maak een html formulier waar een gebruiker zijn basisgegevens in kan opslaan. 
 
3) Maak php code die de gegevens uit het registratie-formulier opslaat in de database. 
 
4) Zorg ervoor dat de gebruiker niet terugkomt op dezelfde pagina na het opslaan van de gegevens. Gebruik hiervoor de volgende code:  
`header('Location: pagina_na_inloggen.php');`
