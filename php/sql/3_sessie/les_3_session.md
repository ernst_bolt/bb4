# Inleiding #
Nadat je bent ingelogd moet de webapplicatie onthouden dat je bent ingelogd. 
 
## Doel 
Het onthouden van gegevens tussen verschillende requests.


# Controle 
Als een gebruiker is ingelogd bewaren we de gebruiker in de sessie. Elk volgend request kijken we of er een sessie is waarin de gebruiker bestaat.   
    
# Opdrachten voor in de les 

1) Voeg een stap toe in je activity: Bewaar gebruiker na inloggen in sessie.

2) Start een sessie 

3) Bewaar de gebruiker in de sessie na een succesvolle inlogpoging.

3) Controleer of de gebruiker in een volgend request aanwezig is in de sessie.


# Huiswerk 
 
Doel: Tel het aantal page_views van een gebruiker.

1) Voeg een page_views key toe aan de sessie.

2) Hoog de waarde van page_views na elk request op.
 
