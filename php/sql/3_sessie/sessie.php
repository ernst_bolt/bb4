<?php

//start altijd de session
session_start();


// http://php.net/manual/en/function.date.php
//Als er een waarde 'nu' in de sessie is, verplaats die dan naar previous in de sessie.
if(isset($_SESSION['nu'])){
	$_SESSION['previous'] = $_SESSION['nu'];
}

//Voeg een waarde 'nu' toe aan de sessie met de huidige datum en tijd (timestamp)
date_default_timezone_set('UTC');
$nu = date('l jS \of F Y h:i:s A');
$_SESSION['nu'] = $nu;

//print de resultaten
echo $_SESSION['previous'];
echo '<br/>';
echo $_SESSION['nu'];

?>