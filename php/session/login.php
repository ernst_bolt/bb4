<?php
session_start();
/*
 * Het basis principe is: versleutel het wachtwoord.
 * 
 * Het wachtwoord wordt verkregen door een string toe te voegen aan de hash-functie.
 * Om de kans te verkleinen dat het algoritme wordt geraden kun je aan de parameter een salt toevoegen.
 * 
 * Om het raden van de salt te verkleinen kun je de salt verlengen met waarde die wijzigt per gebruiker.
 * Bijvoorbeeld de datum van creeren van de gebruiker of de gebruikersnaam.
 * 
 * Winstpunten voor wachtwoord-hashing met een dynamische salt:
 * - wachtwoorden staan niet meer in de database
 * - het raden van de invoerstring wordt verkleind door een salt
 * - een dynamische salt zorg ervoor dat het raden van de salt als parameter moeilijker wordt
 * 
 * 
 * 
 */
$salt = '3ae962e42747548415c09eed6cf9a100';

echo 'POST';
var_dump($_POST);

echo 'SESSION';
var_dump($_SESSION);

echo hash_hmac('md5', 'Hobbes', $salt.'Casper'); 

//md5('Hobbes');

$users = array(
        'Casper' => 'a64652dc579ec364cbc972b44722b939'
    );

if(     isset($_POST['gebruikersnaam']) && $_POST['gebruikersnaam'] =='Casper' &&
        isset($_POST['wachtwoord'])
        ){
    
    $wachtwoord = hash_hmac('md5', $_POST['wachtwoord'], $salt.$_POST['gebruikersnaam']); 
    
    
    if($wachtwoord == $users[$_POST['gebruikersnaam']]){
        $_SESSION['gebruikersnaam'] = $_POST['gebruikersnaam'];
        echo 'U bent ingelogd';
    }
}


?>

<form method="POST">
    Gebruikersnaam<input type="text" name="gebruikersnaam">
    Wachtwoord<input type="password" name="wachtwoord">
    <input type="submit" value="verstuur">
</form>

<a href="logout.php">logout</a>